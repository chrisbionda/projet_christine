<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230111111533 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slot CHANGE morning_time_start morning_time_start TIME NOT NULL, CHANGE morning_time_end morning_time_end TIME NOT NULL, CHANGE ap_time_start ap_time_start TIME NOT NULL, CHANGE ap_time_end ap_time_end TIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slot CHANGE morning_time_start morning_time_start DATETIME NOT NULL, CHANGE morning_time_end morning_time_end DATETIME NOT NULL, CHANGE ap_time_start ap_time_start DATETIME NOT NULL, CHANGE ap_time_end ap_time_end DATETIME NOT NULL');
    }
}
