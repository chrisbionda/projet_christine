<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230112093207 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE prestation DROP FOREIGN KEY FK_51C88FAD809EE01F');
        $this->addSql('DROP TABLE category_prestation');
        $this->addSql('ALTER TABLE booking ADD prestation_id INT NOT NULL');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE9E45C554 FOREIGN KEY (prestation_id) REFERENCES prestation (id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDE9E45C554 ON booking (prestation_id)');
        $this->addSql('ALTER TABLE prestation DROP FOREIGN KEY FK_51C88FAD3301C60');
        $this->addSql('DROP INDEX UNIQ_51C88FAD3301C60 ON prestation');
        $this->addSql('DROP INDEX UNIQ_51C88FAD809EE01F ON prestation');
        $this->addSql('ALTER TABLE prestation DROP booking_id, DROP category_prestation_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category_prestation (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, slug VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE prestation ADD booking_id INT DEFAULT NULL, ADD category_prestation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE prestation ADD CONSTRAINT FK_51C88FAD3301C60 FOREIGN KEY (booking_id) REFERENCES booking (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE prestation ADD CONSTRAINT FK_51C88FAD809EE01F FOREIGN KEY (category_prestation_id) REFERENCES category_prestation (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_51C88FAD3301C60 ON prestation (booking_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_51C88FAD809EE01F ON prestation (category_prestation_id)');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDE9E45C554');
        $this->addSql('DROP INDEX IDX_E00CEDDE9E45C554 ON booking');
        $this->addSql('ALTER TABLE booking DROP prestation_id');
    }
}
