<?php

namespace App\Controller;

use App\Entity\Closing;
use App\Form\ClosingType;
use App\Repository\ClosingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/closing')]
class AdminClosingController extends AbstractController
{
    #[Route('/', name: 'app_admin_closing_index', methods: ['GET'])]
    public function index(ClosingRepository $closingRepository): Response
    {
        return $this->render('admin_closing/index.html.twig', [
            'closings' => $closingRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_closing_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ClosingRepository $closingRepository): Response
    {
        $closing = new Closing();
        $form = $this->createForm(ClosingType::class, $closing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $closingRepository->save($closing, true);

            return $this->redirectToRoute('app_admin_closing_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_closing/new.html.twig', [
            'closing' => $closing,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_closing_show', methods: ['GET'])]
    public function show(Closing $closing): Response
    {
        return $this->render('admin_closing/show.html.twig', [
            'closing' => $closing,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_closing_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Closing $closing, ClosingRepository $closingRepository): Response
    {
        $form = $this->createForm(ClosingType::class, $closing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $closingRepository->save($closing, true);

            return $this->redirectToRoute('app_admin_closing_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_closing/edit.html.twig', [
            'closing' => $closing,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_closing_delete', methods: ['POST'])]
    public function delete(Request $request, Closing $closing, ClosingRepository $closingRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$closing->getId(), $request->request->get('_token'))) {
            $closingRepository->remove($closing, true);
        }

        return $this->redirectToRoute('app_admin_closing_index', [], Response::HTTP_SEE_OTHER);
    }
}
