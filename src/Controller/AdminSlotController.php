<?php

namespace App\Controller;

use App\Entity\Slot;
use App\Form\SlotType;
use App\Repository\SlotRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/slot')]
class AdminSlotController extends AbstractController
{
    #[Route('/', name: 'app_admin_slot_index', methods: ['GET'])]
    public function index(SlotRepository $slotRepository): Response
    {
        return $this->render('admin_slot/index.html.twig', [
            'slots' => $slotRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_slot_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SlotRepository $slotRepository): Response
    {
        $slot = new Slot();
        $form = $this->createForm(SlotType::class, $slot);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slotRepository->save($slot, true);

            return $this->redirectToRoute('app_admin_slot_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_slot/new.html.twig', [
            'slot' => $slot,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_slot_show', methods: ['GET'])]
    public function show(Slot $slot): Response
    {
        return $this->render('admin_slot/show.html.twig', [
            'slot' => $slot,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_slot_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Slot $slot, SlotRepository $slotRepository): Response
    {
        $form = $this->createForm(SlotType::class, $slot);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slotRepository->save($slot, true);

            return $this->redirectToRoute('app_admin_slot_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_slot/edit.html.twig', [
            'slot' => $slot,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_slot_delete', methods: ['POST'])]
    public function delete(Request $request, Slot $slot, SlotRepository $slotRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$slot->getId(), $request->request->get('_token'))) {
            $slotRepository->remove($slot, true);
        }

        return $this->redirectToRoute('app_admin_slot_index', [], Response::HTTP_SEE_OTHER);
    }
}
