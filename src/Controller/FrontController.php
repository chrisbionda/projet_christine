<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Form\BookingType;
use App\Entity\Prestation;
use App\Form\PrestationType;
use App\Form\PrestationChoiceType;
use App\Repository\SlotRepository;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontController extends AbstractController
{
    #[Route('/', name: 'app_accueil')]
    public function index(): Response
    {
        return $this->render('front/index.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/naturopathie', name: 'app_naturopathie')]
    public function naturopathie(): Response
    {
        return $this->render('front/naturopathie.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/reflexologie', name: 'app_reflexologie')]
    public function reflexologie(): Response
    {
        return $this->render('front/reflexologie.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/massage', name: 'app_massage')]
    public function massage(): Response
    {
        return $this->render('front/massage.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/presentation', name: 'app_presentation')]
    public function presentation(): Response
    {
        return $this->render('front/presentation.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/tarifs', name: 'app_tarifs')]
    public function tarifs(): Response
    {
        return $this->render('front/tarifs.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/contact', name: 'app_contact')]
    public function contact(): Response
    {
        return $this->render('front/contact.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/cuisinons', name: 'app_cuisinons')]
    public function cuisinons(): Response
    {
        return $this->render('front/cuisinons.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/retrouver', name: 'app_retrouver')]
    public function retrouver(): Response
    {
        return $this->render('front/retrouver.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/conseils', name: 'app_conseils')]
    public function conseils(): Response
    {
        return $this->render('front/conseils.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/comment', name: 'app_comment')]
    public function comment(): Response
    {
        return $this->render('front/comment.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/consulter', name: 'app_consulter')]
    public function consulter(): Response
    {
        return $this->render('front/consulter.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/principes', name: 'app_principes')]
    public function principes(): Response
    {
        return $this->render('front/principes.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    #[Route('/techniques', name: 'app_techniques')]
    public function techniques(): Response
    {
        return $this->render('front/techniques.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
    
    //#[IsGranted('ROLE_USER')]
    #[Route('/rdv', name: 'app_rdv')]
    public function rdv(Request $request, SlotRepository $slotRepository): Response
    {
        $booking = new Booking();
        
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted()  && $form->isValid()) {
            
            dump($booking->getPrestation()->getTime());
            dump($slotRepository->findAll());
            
        }
        //$today= getdate("Ymd");

        return $this->render('front/rdv.html.twig', [
            'form' => $form->createView()
        ]);

    }

}
