<?php

namespace App\Entity;

use App\Repository\SlotRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SlotRepository::class)]
class Slot
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $numero = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $morningTimeStart = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $morningTimeEnd = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $apTimeStart = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $apTimeEnd = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getMorningTimeStart(): ?\DateTimeInterface
    {
        return $this->morningTimeStart;
    }

    public function setMorningTimeStart(\DateTimeInterface $morningTimeStart): self
    {
        $this->morningTimeStart = $morningTimeStart;

        return $this;
    }

    public function getMorningTimeEnd(): ?\DateTimeInterface
    {
        return $this->morningTimeEnd;
    }

    public function setMorningTimeEnd(\DateTimeInterface $morningTimeEnd): self
    {
        $this->morningTimeEnd = $morningTimeEnd;

        return $this;
    }

    public function getApTimeStart(): ?\DateTimeInterface
    {
        return $this->apTimeStart;
    }

    public function setApTimeStart(\DateTimeInterface $apTimeStart): self
    {
        $this->apTimeStart = $apTimeStart;

        return $this;
    }

    public function getApTimeEnd(): ?\DateTimeInterface
    {
        return $this->apTimeEnd;
    }

    public function setApTimeEnd(\DateTimeInterface $apTimeEnd): self
    {
        $this->apTimeEnd = $apTimeEnd;

        return $this;
    }

}
